package model;
/**
 * @author George Chronis Emmanuel Ankrah
 */
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

import controller.LoginController;

// TODO: Auto-generated Javadoc
/**
 * The Class photoApp, the highest serializable class.
 */
public class photoApp implements Serializable {
	
	/** The Constant storeDir. */
	public static final String storeDir = "Data";
	
	/** The Constant storeFile. */
	public static final String storeFile = "users.data";
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The users. */
	private ArrayList<User> users;
	
	/**
	 * Instantiates a new photo app.
	 */
	public photoApp(){
		users=new ArrayList<User>();
	}
	
	/**
	 * Write app.
	 *
	 * @param logCont the log cont
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static void writeApp( photoApp logCont)
			throws IOException{
				ObjectOutputStream oos = new ObjectOutputStream(
						new FileOutputStream(storeDir+File.separator+storeFile)	);
						oos.writeObject(logCont);
						oos.close();
			}
	
	/**
	 * Read app.
	 *
	 * @return the photo app
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ClassNotFoundException the class not found exception
	 */
	public static photoApp readApp()
			throws IOException, ClassNotFoundException{
				ObjectInputStream ois = new ObjectInputStream(				
					new FileInputStream(storeDir + File.separator + storeFile));
				photoApp log= (photoApp)ois.readObject();
					ois.close();
				return log;
			}
	
	/**
	 * Delete user.
	 *
	 * @param target the target
	 * @return true, if successful
	 */
	public boolean deleteUser(String target){
		for(int i=0; i<users.size();i++){
			//users can't have names that differ only by case
			if(users.get(i).getName().equalsIgnoreCase(target)){
				users.remove(i);
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Adds the user.
	 *
	 * @param user the user
	 * @return true, if successful
	 */
	public boolean addUser(User user){
		//if doesn't already exist in list, 
		for(int i=0; i<users.size();i++){
			//users can't have names that differ only by case
			if(users.get(i).getName().equalsIgnoreCase(user.getName())){
				return false;
			}
		}
		//instantiate and add to user list
		
		users.add(user);
		return true;
	}
	
	/**
	 * Search users.
	 *
	 * @param name the name
	 * @return the user
	 */
	public User searchUsers(String name){
		for(int i =0; i<this.users.size();i++){
			if(this.users.get(i).getName().equals(name)){
				return this.users.get(i) ;
			}
		}
		return null;
	}
	
	/**
	 * Gets the users.
	 *
	 * @return the users
	 */
	public ArrayList<User> getUsers() {
		return users;
	}
	
	/**
	 * Sets the users.
	 *
	 * @param users the new users
	 */
	public void setUsers(ArrayList<User> users) {
		this.users = users;
	}
	
	/**
	 * User list to string.
	 *
	 * @param Users the users
	 * @return the array list
	 */
	public ArrayList<String> userListToString(ArrayList<User> Users){
		ArrayList<String> stringList = new ArrayList<String>();
		for(int i = 0; i < Users.size(); i++){
			stringList.add(Users.get(i).getName());
		}
		return stringList;
	}
}
