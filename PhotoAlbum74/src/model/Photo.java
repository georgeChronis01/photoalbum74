package model;
/**
 * @author George Chronis Emmanuel Ankrah
 */
import java.io.File;
import java.io.Serializable;

import java.util.ArrayList;
import java.util.Calendar;


import javafx.scene.image.Image;

// TODO: Auto-generated Javadoc
/**
 * The Class Photo.
 */
public class Photo implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The main image to be displayed in slideshow. */
	private transient Image mainImage;
	
	/** The thumbnail version of image. */
	private transient Image thumbnail;
	
	/** The filename. */
	private String filename;
	
	/** The file. */
	private File file;
	
	/** The tags. */
	private ArrayList <Tag> tags;
	
	/** The calendar. */
	private Calendar calendar;
	
	/** The caption. */
	private String caption;
	
	/** The valid. */
	private boolean valid;
	
	/**
	 * Instantiates a new photo.
	 *
	 * @param url the url
	 */
	public Photo(String url){
		this.file = new File(url);
		this.setMainImage(new Image(file.toURI().toString(), 274, 155, true, true));
		this.setThumbnail(new Image(file.toURI().toString(), 55, 40, true, true));
		this.setFilename(url);
		setTags(new ArrayList<Tag>());
		
		//need to worry about milliseconds?
		Calendar cal = Calendar.getInstance();
		// cal.setTimeInMillis(0);
		 cal.set(Calendar.MILLISECOND,0);
		 this.setDate(cal);
		this.getDate();
		this.setCaption(null);
		//SHOULD PROBABLY CHECK IF FILE EXISTS
		if(file.exists() && !file.isDirectory()) { 
			this.setValid(true);
		}
		else{
			this.setValid(false);
		}
	}
	
	/**
	 * Gets the image from directory when program starts because image class is not serializable. 
	 *
	 * @return the image from dir
	 */
	//can't serialize images, have to recreate every time you log in
	public Image getImageFromDir (){
		//make file, check if null
		File file = new File(this.filename);
		if(file.exists() && !file.isDirectory()) { 
			this.setMainImage(new Image(file.toURI().toString(), 274, 155, true, true));
			this.setThumbnail(new Image(file.toURI().toString(), 55, 40, true, true));
		}
		else{
		
		}
		this.setMainImage(new Image(file.toURI().toString(), 274, 155, true, true));
		this.setThumbnail(new Image(file.toURI().toString(), 55, 40, true, true));
		return null;
	}
	
	/**
	 * Resize image.
	 *
	 * @param width the width
	 * @param height the height
	 */
	public void resize(int width, int height){
		//throw out current image, initialize
		this.mainImage=new Image(this.file.toURI().toString(), width, height, true, true);
	}
	
	/**
	 * Adds the tag.
	 *
	 * @param tag the tag
	 * @return true, if successful
	 */
	public boolean addTag(Tag tag){
		//if not a duplicate add
		
		for(int i =0; i<this.tags.size();i++){
			Tag rem=null;
			if(tags.get(i).getType().equals(tag.getType())&&tags.get(i).getValue().equals(tag.getValue())){
				return false;
			}
		}
		tags.add(tag);
		return true;
	}

/**
 * Removes the tag.
 *
 * @param tag the tag
 * @return the tag
 */
public Tag removeTag(Tag tag){
	//if it exists
	for(int i =0; i<this.tags.size();i++){
		Tag rem=null;
		if(tags.get(i).getType().equals(tag.getType())&&tags.get(i).getValue().equals(tag.getValue())){
			rem =tags.remove(i);
			return rem;
		}
	}
	return null;
}
	
	/**
	 * Gets the caption.
	 *
	 * @return the caption
	 */
	public String getCaption() {
		return caption;
	}
	
	/**
	 * Sets the caption.
	 *
	 * @param caption the new caption
	 */
	public void setCaption(String caption) {
		this.caption = caption;
	}
	
	/**
	 * Gets the main image.
	 *
	 * @return the main image
	 */
	public Image getMainImage() {
		return mainImage;
	}
	
	/**
	 * Sets the main image.
	 *
	 * @param image the new main image
	 */
	public void setMainImage(Image image) {
		this.mainImage = image;
	}
	
	/**
	 * Gets the tags.
	 *
	 * @return the tags
	 */
	public ArrayList <Tag> getTags() {
		return tags;
	}
	
	/**
	 * Sets the tags.
	 *
	 * @param tags the new tags
	 */
	public void setTags(ArrayList <Tag> tags) {
		this.tags = tags;
	}
	
	/**
	 * Gets the date.
	 *
	 * @return the date
	 */
	public Calendar getDate() {
		return calendar;
	}
	
	/**
	 * Sets the date.
	 *
	 * @param date the new date
	 */
	public void setDate(Calendar date) {
		this.calendar = date;
	}
	
	/**
	 * Gets the filename.
	 *
	 * @return the filename
	 */
	public String getFilename() {
		return filename;
	}
	
	/**
	 * Sets the filename.
	 *
	 * @param filename the new filename
	 */
	public void setFilename(String filename) {
		this.filename = filename;
	}
	
	/**
	 * Gets the thumbnail.
	 *
	 * @return the thumbnail
	 */
	public Image getThumbnail() {
		return thumbnail;
	}
	
	/**
	 * Sets the thumbnail.
	 *
	 * @param thumbnail the new thumbnail
	 */
	public void setThumbnail(Image thumbnail) {
		this.thumbnail = thumbnail;
	}
	
	/**
	 * Checks if filename is valid.
	 *
	 * @return true, if is valid
	 */
	public boolean isValid() {
		return valid;
	}
	
	/**
	 * Sets the valid.
	 *
	 * @param valid the new valid
	 */
	public void setValid(boolean valid) {
		this.valid = valid;
	}
	
	/**
	 * Tags to string.
	 *
	 * @return the string
	 */
	public String tagsToString(){
		String ret=""; 
		for(int i =0; i<this.tags.size();i++){
			if(i==0){
				ret= tags.get(i).toString();
				continue;
			}
			ret= " , "+ tags.get(i).toString();
		}
		return ret;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		return this.filename;
	}
}
