package model;
/**
 * @author George Chronis Emmanuel Ankrah
 */
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * The Class Album.
 */
public class Album implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The name. */
	private String name;
	
	/** The oldest photo date. */
	private Date oldestDate;
	
	/** The newest photo date. */
	private Date newestDate;
	
	/** The photos. */
	private ArrayList<Photo> photos;
	
	/** The number of photos. */
	private int numberOfPhotos;
	
	/** The calendar of oldest photo. */
	private Calendar dateOldestPhoto;
	
	/** The calendar ofnewest photo. */
	private Calendar dateNewestPhoto;
	
	/**
	 * Instantiates a new album. Dates and calendars set to null because there are no pictures.
	 *
	 * @param name the name
	 */
	public Album(String name){
		this.name=name;
		this.photos  = new ArrayList<Photo>();
		this.numberOfPhotos=0;
		this.dateOldestPhoto=null;
		this.dateNewestPhoto=null;
		this.setNewestDate(null);
		this.setOldestDate(null);
	}
	
	/**
	 * Find newest photo.
	 *
	 * @return the photo
	 */
	public Photo findNewestPhoto(){
		Photo late=photos.get(0);
		for(int i=0;i<photos.size();i++){
			if(photos.get(i).getDate().after(late.getDate())){
				late=photos.get(i);
			}
		}
		this.dateNewestPhoto=late.getDate();
		this.setNewestDate(late.getDate().getTime());
		return late;
	}
	
	/**
	 * Find oldest photo.
	 *
	 * @return the photo
	 */
	public Photo findOldestPhoto(){
		Photo late=photos.get(0);
		for(int i=0;i<photos.size();i++){
			if(photos.get(i).getDate().before(late.getDate())){
				late=photos.get(i);
			}
		}
		this.dateOldestPhoto=late.getDate();
		this.setOldestDate(late.getDate().getTime());
		return late;
	}
	
	/**
	 * Adds the photo to album
	 *
	 * @param photo the photo
	 * @return true, if successful
	 */
	public boolean addPhoto(Photo photo){
		//assuming we don't have to check if the filename is valid
		//just check for duplicates
		//if file name is not valid, do not add
		//if it is not added, then in effect it does not really exist
		if(!photo.isValid()) return false;
		for(int i=0;i<photos.size();i++){ // if there's another album with that name, can't rename this album to it
			if(photos.get(i).getFilename().equalsIgnoreCase(photo.getFilename())){
				return false;
			}
		}
		//instantiate and add
		//Photo add = new Photo(filename);
		//EVERY TIME YOU ADD A NEW PHOTO HAVE TO CHECK IF OLDEST AND NEWEST PHOTOS CHANGE
		//IF SO CHANGE THE FIELDS
		photos.add(photo);
		this.numberOfPhotos++;
		this.dateNewestPhoto=photo.getDate();
		findNewestPhoto();
		findOldestPhoto();
		//System.out.println(photo.getDate().getTime());

		return true;
	}
	
	/**
	 * Delete photo from album.
	 *
	 * @param photo the photo
	 * @return true, if successful
	 */
	public boolean deletePhoto(Photo photo){
		//EVERY TIME YOU ADD A NEW PHOTO HAVE TO CHECK IF OLDEST AND NEWEST PHOTOS CHANGE
				//IF SO CHANGE THE FIELDS
		//find 
		if(photos.contains(photo)){
			photos.remove(photo);
			this.numberOfPhotos--;
		//update dates
			//if there are photos
			if(photos.size()>0){
				findOldestPhoto();
				findNewestPhoto();
			}
			return true;
		}
		//if not found can't delete
		return false;
	}
	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Gets the photos.
	 *
	 * @return the photos
	 */
	public ArrayList<Photo> getPhotos() {
		return photos;
	}
	
	/**
	 * Sets the photos.
	 *
	 * @param photos the new photos
	 */
	public void setPhotos(ArrayList<Photo> photos) {
		this.photos = photos;
	}
	
	/**
	 * Gets the number of photos.
	 *
	 * @return the number of photos
	 */
	public int getNumberOfPhotos() {
		return numberOfPhotos;
	}
	
	/**
	 * Sets the number of photos.
	 *
	 * @param numberOfPhotos the new number of photos
	 */
	public void setNumberOfPhotos(int numberOfPhotos) {
		this.numberOfPhotos = numberOfPhotos;
	}
	
	/**
	 * Gets the date oldest photo.
	 *
	 * @return the date oldest photo
	 */
	public Calendar getDateOldestPhoto() {
		return dateOldestPhoto;
	}
	
	/**
	 * Sets the date oldest photo.
	 *
	 * @param date the new date oldest photo
	 */
	public void setDateOldestPhoto(Calendar date) {
		this.dateOldestPhoto = date;
	}
	
	/**
	 * Gets the date newest photo.
	 *
	 * @return the date newest photo
	 */
	public Calendar getDateNewestPhoto() {
		return dateNewestPhoto;
	}
	
	/**
	 * Sets the date newest photo.
	 *
	 * @param date the new date newest photo
	 */
	public void setDateNewestPhoto(Calendar date) {
		this.dateNewestPhoto = date;
	}
	
	/**
	 * Date to string.
	 *
	 * @param cal the cal
	 * @return the string
	 */
	public String dateToString(Calendar cal){
		return ""+ cal;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		return this.name;
	}
	
	/**
	 * Prints out photo list.
	 *
	 * @return the string
	 */
	public String photosToString(){
		String ret=""; 
		for(int i =0; i<this.photos.size();i++){
			if(i==0){
				ret= photos.get(i).toString();
				continue;
			}
			ret=" , "+ photos.get(i).toString();
		}
		return ret;
	}
	
	/**
	 * Gets the oldest date.
	 *
	 * @return the oldest date
	 */
	public Date getOldestDate() {
		return oldestDate;
	}
	
	/**
	 * Sets the oldest date.
	 *
	 * @param oldestDate the new oldest date
	 */
	public void setOldestDate(Date oldestDate) {
		this.oldestDate = oldestDate;
	}
	
	/**
	 * Gets the newest date.
	 *
	 * @return the newest date
	 */
	public Date getNewestDate() {
		return newestDate;
	}
	
	/**
	 * Sets the newest date.
	 *
	 * @param date the new newest date
	 */
	public void setNewestDate(Date date) {
		this.newestDate = date;
	}
	
}
