package model;
/**
 * @author George Chronis Emmanuel Ankrah
 */
import java.io.Serializable;
import java.util.ArrayList;

// TODO: Auto-generated Javadoc
/**
 * The Class User.
 */
public class User implements Serializable{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The name. */
	private String name;
	
	/** The albums. */
	private ArrayList<Album> albums;
	
	/**
	 * Instantiates a new user.
	 *
	 * @param name the name
	 */
	public User(String name){
		this.setName(name);
		this.albums =new ArrayList<Album>();
	}
	
	/**
	 * Adds the album.
	 *
	 * @param albumName the album name
	 * @return the album
	 */
	public Album addAlbum(String albumName){
		//if no duplicate name
		for(int i=0;i<albums.size();i++){ // if there's another album with that name, can't rename this album to it
			if(albums.get(i).getName().equalsIgnoreCase(albumName)){
				return null;
			}
		}
		//instantiate and add
		Album add = new Album(albumName);
		albums.add(add);
		return add;
	}
	
	/**
	 * Delete album.
	 *
	 * @param album the album
	 * @return the album
	 */
	public Album deleteAlbum(Album album){
		//find 
		if(albums.contains(album)){
			albums.remove(album);
			return album;
		}
		//if not found can't delete
		return null;
	}
	
	/**
	 * Rename album.
	 *
	 * @param album the album
	 * @param newName the new name
	 * @return the album
	 */
	public Album renameAlbum(Album album,  String newName){
		//find
		int i;
		for(i=0;i<albums.size();i++){ // if there's another album with that name, can't rename this album to it
			if(albums.get(i).getName().equalsIgnoreCase(newName)){
				return null;
			}
		}
		//rename
		album.setName(newName);
		return album ;
	}
	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		return this.getName();
	}
	
	/**
	 * Gets the albums.
	 *
	 * @return the albums
	 */
	public ArrayList<Album> getAlbums() {
		return albums;
	}
	
	/**
	 * Sets the albums.
	 *
	 * @param albums the new albums
	 */
	public void setAlbums(ArrayList<Album> albums) {
		this.albums = albums;
	}
}
