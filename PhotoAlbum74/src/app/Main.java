package app;
/**
 * @author George Chronis Emmanuel Ankrah
 */
import controller.MainWindowController;
import controller.SearchController;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import controller.LoginController;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;

import model.User;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;

// TODO: Auto-generated Javadoc
/**
 * The Class Main.
 */
public class Main extends Application {
	
	
	/* (non-Javadoc)
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	@Override
	public void start(Stage primaryStage) {
		try {
			//either make multiple scenes or ...multiple stages 
			FXMLLoader loader = new FXMLLoader();   
			loader.setLocation(


				//getClass().getResource("/view/project2MainWindow.fxml"));

					//getClass().getResource("/view/project2SearchWindow.fxml"));

			//getClass().getResource("/view/project2PhotoViewWindow.fxml"));

					getClass().getResource("/view/login.fxml"));


			AnchorPane root = (AnchorPane)loader.load();
		
	
					User user = new User("Test");

			//MainWindowController listController = loader.getController();
			//PhotoWindowController listController = loader.getController();

			//listController.start(primaryStage, ad);

//			SearchController listController = loader.getController();
//			listController.start(primaryStage, user, ad);
			//Scene scene = new Scene(root,800,600);
			//scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());

			LoginController adminController = loader.getController();
			adminController.start(primaryStage);
			
			Scene scene = new Scene(root,600,400);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());

			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	//serialization writer
	//i guess before every log out and quit have to write??
	//OR should this go in the user class?
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		launch(args);
		//writeApp();
	}
}




