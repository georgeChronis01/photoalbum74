package controller;
/**
 * @author George Chronis Emmanuel Ankrah
 */
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.Node;
import model.User;
import model.photoApp;
import java.io.IOException;
import java.util.ArrayList;

import controller.AdminController;

// TODO: Auto-generated Javadoc
/**
 * The Class LoginController.
 */
public class LoginController {

	//written to file, not serialized, so you can login without serializing

//	//HAVE TO IMPLEMENT THIS
//	private transient ArrayList<String> userNames; 
	
	/** The this app. */
	private photoApp thisApp;
	//serialized stuff, just need this since users contains everything else
	//public ArrayList<User>users;
	/** The tf username. */
	//not serializing logincontroller because you need to instantiate it and you can't do that inside here...or maybe you could, just using aggregator class
	@FXML TextField tfUsername;
	
	/** The login. */
	@FXML Button login;
	
	/** The this user. */
	private User thisUser;
	
	/**
	 * Start.
	 *
	 * @param mainStage the main stage
	 */
	//public Admin thisAdmin;
	public void start(Stage mainStage){
		thisApp=new photoApp();
		

		try {
			thisApp = photoApp.readApp();
			photoApp.readApp();
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			//e2.printStackTrace();
		}
		
//		Admin admin= new Admin();
//		thisAdmin=admin;
	}
	
	/**
	 * Logs into admin view or album view.
	 *
	 * @param e the action event
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@FXML
	public void buttonHandleAction(ActionEvent e) throws IOException{
	String name =tfUsername.getText();
	//System.out.println(thisApp);
		if(name.equalsIgnoreCase("admin")){
			Stage app_stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
			
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/view/AdminSystem.fxml"));
			AnchorPane root = (AnchorPane) loader.load();
			
			AdminController adminController = loader.getController();
		      adminController.start(app_stage, thisApp);

		      Scene scene = new Scene(root, 600, 358);
		      app_stage.setScene(scene);
		      app_stage.show(); 
		      app_stage.setTitle("Admin");
		      app_stage.setResizable(false);
		}
		else if(thisApp.searchUsers(name)!=null)
		{
			thisUser=thisApp.searchUsers(name);
			Stage app_stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/view/project2MainWindow.fxml"));
			AnchorPane root = (AnchorPane) loader.load();
			
			MainWindowController mainController = loader.getController();
		      mainController.start(app_stage, thisApp, thisUser);
		      Scene scene = new Scene(root, 600, 400);
		      app_stage.setScene(scene);
		      app_stage.show(); 
		      app_stage.setTitle("Albums View");
		      app_stage.setResizable(false);
		}
		else{
//			TextField tf = new TextField("Invalid Username");
//			tf.setLayoutX(300);
//			tf.setLayoutY(300);
			Alert alertOne = new Alert(AlertType.ERROR);
			//alert.initOwner(mainStage);
		    alertOne.setTitle("Invalid Username");
		    alertOne.setHeaderText(  "Invalid Username");
		    alertOne.setContentText("Invalid Username");
		   	alertOne.showAndWait();
		}
	}
}
