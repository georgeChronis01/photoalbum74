package controller;
/**
 * @author George Chronis Emmanuel Ankrah
 */
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import model.Album;
import model.User;
import model.photoApp;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

// TODO: Auto-generated Javadoc
/**
 * The Class MainWindowController.
 */
public class MainWindowController {
	 
 	/** The safe quit. */
 	@FXML Button safeQuit;
	 
 	/** The add album. */
 	@FXML Button addAlbum;
	 
 	/** The search. */
 	@FXML Button search;
	 
 	/** The delete album. */
 	@FXML Button deleteAlbum;
	 
 	/** The rename album. */
 	@FXML Button renameAlbum;
	 
 	/** The open album. */
 	@FXML Button openAlbum;
	 
 	/** The sign out. */
 	@FXML Button signOut;
	 
 	/** The album list. */
 	@FXML TableView<Album> albumList;
	 
 	/** The album column. */
 	@FXML TableColumn<Album, String> albumCol;
	  
 	/** The date column . */
 	@FXML TableColumn<Album, Date> dateCol;
	 
 	/** The range column. */
 	@FXML TableColumn<Album, Date> rangeCol;
	 
 	/** The num column. */
 	@FXML TableColumn<Album, Integer> numCol;
	  
 	/** The add album field. */
 	@FXML TextField addAlbumField;
	 
 	/** The rename album field. */
 	@FXML TextField renameAlbumField;
	
	/** The this app. */
	// public Admin thisAdmin;
 	private photoApp thisApp;
	   
   	/** The main. */
   	Stage main;
	   
   	/** The this user. */
   	private User thisUser;
	   
   	/** The obs list. */
   	private ObservableList<Album> obsList;              
	  //test albums for a given user
	   //user.getAlbums().add(new Album("NEW ALBUM"));
	  /**
  	 * Start.
  	 *
  	 * @param mainStage the main stage
  	 * @param app the app
  	 * @param user the user
  	 */
  	//not sure if controller or model or main should actually take in songs -i/o
	   public void start(Stage mainStage, photoApp app, User user) {
		   thisUser=user;
		   thisApp=app;
		   //ALSO SET RESIZABLENESS OF WINDOWS AND TABLE COLUMNS
		   //
		   main = mainStage;
//		   thisUser= new User("Bam");
//		  User  xUser= new User("Ham");
//		   User yUser= new User("Wham");
//		   thisApp.addUser(thisUser);
//		   thisApp.addUser(xUser);
//		   thisApp.addUser(yUser);
		   
//		   thisUser.addAlbum("a");
//		   thisUser.addAlbum("b");
//		   thisUser.addAlbum("c");
//		   thisUser.addAlbum("d");
//		   thisUser.addAlbum("e");
//		   thisUser.getAlbums().get(0).setNumberOfPhotos(0);
//		   thisUser.getAlbums().get(1).setNumberOfPhotos(1);
//		   thisUser.getAlbums().get(2).setNumberOfPhotos(2);
//		   thisUser.getAlbums().get(3).setNumberOfPhotos(3);
//		   thisUser.getAlbums().get(4).setNumberOfPhotos(4);
//		   Calendar cal = Calendar.getInstance();
//		   cal.setTimeInMillis(0);
//		   cal.set(2016, 1, 1, 1, 1, 1);
//		   Date date = cal.getTime(); // get back a Date object
//		   thisUser.getAlbums().get(0).setDateNewestPhoto(cal);
//		   //date.
//		   thisUser.getAlbums().get(1).setDateNewestPhoto(cal);
//		   thisUser.getAlbums().get(2).setDateNewestPhoto(cal);
//		   thisUser.getAlbums().get(3).setDateNewestPhoto(cal);
//		   thisUser.getAlbums().get(4).setDateNewestPhoto(cal);
//		   
//		   thisUser.getAlbums().get(0).setDateOldestPhoto(cal);
//		   thisUser.getAlbums().get(1).setDateOldestPhoto(cal);
//		   thisUser.getAlbums().get(2).setDateOldestPhoto(cal);
//		   thisUser.getAlbums().get(3).setDateOldestPhoto(cal);
//		   thisUser.getAlbums().get(4).setDateOldestPhoto(cal);
		   
		   ArrayList<Album> usersAlbums = new ArrayList<Album>();
		   for(int i=0;i<thisUser.getAlbums().size();i++){
			   usersAlbums.add(thisUser.getAlbums().get(i));
		   }
		   obsList = FXCollections.observableArrayList(  
				   usersAlbums
//				   thisUser.getAlbums().get(0),
//				   thisUser.getAlbums().get(1),
//				   thisUser.getAlbums().get(2),
//				   thisUser.getAlbums().get(3),
//				   thisUser.getAlbums().get(4)
				   );   
		   albumCol.setCellValueFactory(
				    new PropertyValueFactory<Album, String>("name")
				);
			dateCol.setCellValueFactory(
			    new PropertyValueFactory<Album, Date>("oldestDate")
			);
			rangeCol.setCellValueFactory(
			    new PropertyValueFactory<Album, Date>("newestDate")
			);
			numCol.setCellValueFactory(
				    new PropertyValueFactory<Album, Integer>("numberOfPhotos")
			);
				albumList.setItems(obsList);
				albumCol.setResizable(true);
				albumCol.setResizable(true);
				albumCol.setResizable(true);
				albumCol.setResizable(true);
				albumList.getSelectionModel().select(0);
				//albumList.getColumns().addAll(albumCol,  numCol);
		   //1. load albums for specific user who logged in and list them in table view
		   //2. provide add, delete,    methods
		   //3. allow log out, safequit, and blub
		   
		   //present albums in table view
		   //need to loop though every album/ item in obslist
		   //albumList.getColumns().add
		  mainStage.setTitle("User's Album");
	   }
	   
   	/**
   	 * Safe quit.
   	 *
   	 * @param e the action event
   	 */
   	@FXML
	public void safeQuit(ActionEvent e){
		   Alert alert = new Alert(AlertType.CONFIRMATION);
			 alert.setTitle("Confirmation Dialog");
			 alert.setContentText("Are you sure?");
			 Optional<ButtonType> result = alert.showAndWait();
			 if(result.get() == ButtonType.OK){
				 //serialize!!!!
				 try {
					photoApp.writeApp(thisApp);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				 Platform.exit();
				 System.exit(0);
			 }
	}
	   
   	/**
   	 * Sign out.
   	 *
   	 * @param e the action event
   	 */
   	@FXML
	public void signOut(ActionEvent e){
		   //how do you sign out???
		   //serialize 
		   try {
				photoApp.writeApp(thisApp);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		   //just return to log in window
		   Alert alert = new Alert(AlertType.CONFIRMATION);
			 alert.setTitle("Confirmation Dialog");
			 alert.setContentText("Are you sure?");
			 
			 Optional<ButtonType> result = alert.showAndWait();
			 if(result.get() == ButtonType.OK){
				 //write everything to serializer
				 //writeApp();
				 Stage app_stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
					
					FXMLLoader loader = new FXMLLoader();
					loader.setLocation(getClass().getResource("/view/login.fxml"));
					AnchorPane root = null;
					try {
						root = (AnchorPane) loader.load();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					LoginController loginController = loader.getController();
				      loginController.start(app_stage);

				      Scene scene = new Scene(root, 661, 407);
				      app_stage.setResizable(false);
				      app_stage.setScene(scene);
				      app_stage.show(); 
				      app_stage.setTitle("Log in");
			 }
		   
	   }
	   
   	/**
   	 * Adds the album to user's album list.
   	 *
   	 * @param e the action event
   	 */
   	@FXML
	public void addAlbum(ActionEvent e){
		 //have confirmation for name
		   Alert alert = new Alert(AlertType.CONFIRMATION);
			 alert.setTitle("Confirmation Dialog");
			 alert.setContentText("Add " +addAlbumField.getText() +" ?");
			 Optional<ButtonType> result = alert.showAndWait();
			//check for duplicate, if not,  instantiate and add to current user's list, all done in method
			 Album add=null;
			 if(result.get() == ButtonType.OK){
				 	add=thisUser.addAlbum(addAlbumField.getText());
			 }
			 else{
				 return;
			 }
			 if(add!=null){
				//HAVE TO UPDATE TABLE VIEW if not using properties
				 obsList.add(add);
				 int x = obsList.size();
				 x--;
				albumList.setItems(obsList);
				albumList.getSelectionModel().select(x);
				return;
			 }
			 //was a duplicate
			 else{
				 Alert alertOne = new Alert(AlertType.INFORMATION);
					//alert.initOwner(mainStage);
				    alertOne.setTitle("Add album error");
				    alertOne.setHeaderText(  "Album already exists");
				    alertOne.setContentText("Album already exists");
				   	alertOne.showAndWait();
			 }
	}
	   
   	/**
   	 * Delete album from user's album list.
   	 *
   	 * @param e the action event
   	 */
   	@FXML
	public void deleteAlbum(ActionEvent e){
		//have confirmation
		   Alert alert = new Alert(AlertType.CONFIRMATION);
			 alert.setTitle("Confirmation Dialog");
			 alert.setContentText("Delete " +albumList.getSelectionModel().getSelectedItem()  + "?");
			 Optional<ButtonType> result = alert.showAndWait();
			 //delete
			 Album delete=null;
			 if(result.get() == ButtonType.OK){
				 	delete=thisUser.deleteAlbum(albumList.getSelectionModel().getSelectedItem());
			 }
			 else{
				 return;
			 }
			 if(delete==null){
				 Alert alertOne = new Alert(AlertType.INFORMATION);
					//alert.initOwner(mainStage);
				    alertOne.setTitle("Delete album error");
				    alertOne.setHeaderText(  "Album doesn't exist");
				    alertOne.setContentText("Album doesn't exist");
				   	alertOne.showAndWait();
				   	return;
			 }
		 //HAVE TO UPDATE TABLE VIEW
			 obsList.remove(delete);
		   albumList.setItems(obsList);
	}
	   
   	/**
   	 * Rename album.
   	 *
   	 * @param e the action event
   	 */
   	@FXML	
	   public void renameAlbum(ActionEvent e){
		//have confirmation
		   Album delete = albumList.getSelectionModel().getSelectedItem();
		   Alert alert = new Alert(AlertType.CONFIRMATION);
			 alert.setTitle("Confirmation Dialog");
			 alert.setContentText("Rename " +albumList.getSelectionModel().getSelectedItem()  + " to " + renameAlbumField.getText()+ "?");
			 Optional<ButtonType> result = alert.showAndWait();
			 Album newAlbum=null;
			 if(result.get() == ButtonType.OK){
				 newAlbum = thisUser.renameAlbum( albumList.getSelectionModel().getSelectedItem(), renameAlbumField.getText());
			 }
			 else{
				 return;
			 }
			 if(newAlbum==null){
				 Alert alertOne = new Alert(AlertType.INFORMATION);
					//alert.initOwner(mainStage);
				    alertOne.setTitle("Rename album error");
				    alertOne.setHeaderText(  "Album name taken");
				    alertOne.setContentText("Album name taken");
				   	alertOne.showAndWait();
				   	return;
			 }
		  //have to delete old and add new?
		 //HAVE TO UPDATE TABLE VIEW
			 obsList.remove(delete);
			 obsList.add(newAlbum);
		   albumList.setItems(obsList);
	}
	   
   	/**
   	 * Search for pictures. Opens new window.
   	 *
   	 * @param e the action event
   	 */
   	@FXML
	public void search(ActionEvent e){
		//switch over to search window
		   Stage app_stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
			
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/view/project2SearchWindow.fxml"));
			AnchorPane root = null;
			try {
				root = (AnchorPane) loader.load();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			SearchController pwController = loader.getController();
		      try {
				pwController.start(app_stage, thisUser, thisApp);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		      Scene scene = new Scene(root, 600, 358);
		      app_stage.setResizable(false);
		      app_stage.setScene(scene);
		      app_stage.show(); 
		      app_stage.setTitle("Admin");
	}
	   
   	/**
   	 * Open album. Opens new window to thumbnail view.
   	 *
   	 * @param e the action event
   	 */
   	@FXML
	public void openAlbum(ActionEvent e){
		   //do this if something is selected
		   if(albumList.getSelectionModel().getSelectedItem()!=null){
			//switch over to photoview window
			   Stage app_stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
				
				FXMLLoader loader = new FXMLLoader();
				loader.setLocation(getClass().getResource("/view/project2PhotoViewWindow.fxml"));
				AnchorPane root = null;
				try {
					root = (AnchorPane) loader.load();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				PhotoWindowController pwController = loader.getController();
			      pwController.start(app_stage, albumList.getSelectionModel().getSelectedItem(), thisApp, thisUser);
	
			      Scene scene = new Scene(root, 600, 500);
			      app_stage.setResizable(false);
			      app_stage.setScene(scene);
			      app_stage.show(); 
			      app_stage.setTitle("Admin");
		   }
		   else{
			   Alert alerto = new Alert(AlertType.ERROR);
				 //alert.initOwner(mainStage);
			       alerto.setTitle("No album selected");
			       alerto.setHeaderText(  "No album selected");
			       alerto.setContentText("No album selected");
			   		alerto.showAndWait();
			   		return;
		   }
	}
}
