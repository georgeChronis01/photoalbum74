package controller;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ListView;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.AnchorPane;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;



import model.Album;
import model.User;
import model.photoApp;
import model.Photo;

import model.Tag;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Optional;
import java.util.jar.Attributes.Name;

// TODO: Auto-generated Javadoc
/**
 * The Class SearchController.
 */
public class SearchController {

	/** The create button. */
	@FXML Button createButton;
	
	/** The back button. */
	@FXML Button backButton;
	
	/** The search button. */
	@FXML Button searchButton;
	
	/** The log out button. */
	@FXML Button logOutButton;
	
	/** The safe quit button. */
	@FXML Button safeQuitButton;
	
	/** The search date check box. */
	@FXML CheckBox searchDateCheckBox;
	
	/** The search tag check box. */
	@FXML CheckBox searchTagCheckBox;
	
	/** The criteria 1. */
	@FXML TextField criteria1;
	
	/** The criteria 2. */
	@FXML TextField criteria2;
	
	/** The list view. */
	@FXML ListView<String> listView;
	
	/** The search. */
	Stage search;
	
	/** The this user. */
	User thisUser;
	
	/** The this app. */
	public photoApp thisApp;
	
	/** The searching on tags. */
	public boolean searchingOnTags;
	
	/** The searching on dates. */
	public boolean searchingOnDates;
	
	/** The obs list. */
	private ObservableList<String> obsList;
	
	/** The all photos. */
	public ArrayList<Photo> allPhotos ;
	
//	public Admin thisAdmin;
	
	
	
	/**
 * Start.
 *
 * @param mainStage the main stage
 * @param user the user
 * @param app the app
 * @throws IOException Signals that an I/O exception has occurred.
 */
public void start(Stage mainStage, User user, photoApp app) throws IOException{
		thisApp=app;
		search = mainStage;
		thisUser = user;
		obsList = FXCollections.observableArrayList();
		
		// retrieve all albums from user and convert 
		ArrayList<Album> userAlbums = user.getAlbums();
		
		// create arraylist of all photos that belong to the user for searching
		allPhotos = new ArrayList<Photo>();
		allPhotos.addAll(allUserPhotos(userAlbums));
		
		
		searchDateCheckBox.setOnAction(e -> handleButtonAction(e));
		searchTagCheckBox.setOnAction(e -> handleButtonAction(e));
		
		
		Album a = new Album("a");
		Album b = new Album("b");
		Album c = new Album("c");
		
 		Photo test1 = new Photo ("C:/Users/Rel/Pictures/image.jpg");
		Photo test2 = new Photo ("C:/Users/Rel/Pictures/image1.jpg");
		Photo test3 = new Photo ("C:/Users/Rel/Pictures/image2.jpg");
		Photo test4 = new Photo ("C:/Users/Rel/Pictures/image3.jpg");
		
		ArrayList<Tag> testTag = new ArrayList<Tag>();
		Tag tagtest = new Tag("Location", "Chicago");
		testTag.add(tagtest);
		ArrayList<Tag> wrongTags = new ArrayList<Tag>();
		Tag otherTag= new Tag("Location", "nope");
		wrongTags.add(otherTag);
		test1.setCaption("test image 1");
		test1.setTags(testTag);
		test2.setCaption("test image 2");
		test2.setTags(testTag);
		test3.setCaption("test image 3");
		test4.setCaption("test image 4");
		test3.setTags(wrongTags);
		test4.setTags(wrongTags);
		
		
		allPhotos.add(test1);
		allPhotos.add(test2);
		allPhotos.add(test3);
		allPhotos.add(test4);
	//obsList.addAll(allPhotos);
//		obsList.add(test1.getFilename());
//		obsList.add(test2.getFilename());
//		obsList.add(test3.getFilename());
//		obsList.add(test4.getFilename());
		
		listView.setItems(obsList);
		listView.getSelectionModel().select(0);
		
		//Listener
		listView
		.getSelectionModel()
		.selectedItemProperty()
		.addListener(
				(obs, oldVal, newVal) ->
				showItemInputDialog(mainStage));
		
	}
	
	/**
	 * Show item input dialog.
	 *
	 * @param mainStage the main stage
	 */
	@FXML
	public void showItemInputDialog(Stage mainStage){
		listView.getSelectionModel().getSelectedItem();
		listView.getSelectionModel().getSelectedIndex();
	}
	
	/**
	 * Creates the album.
	 *
	 * @param e the e
	 */
	@FXML
	public void createAlbum(ActionEvent e){
		// create album from search results
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Confirmation Dialog");
		alert.setContentText("Create album from search results?");
		
		Optional<ButtonType> result = alert.showAndWait();
		if(result.get() == ButtonType.OK){
			Album newAlbum = new Album("Search Album");
			newAlbum.setPhotos(allPhotos);
			thisUser.getAlbums().add(newAlbum);
		}
	}
	
	/**
	 * Back.
	 *
	 * @param e the e
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@FXML
	public void back(ActionEvent e) throws IOException{
		// go back to album view screen
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Confirmation Dialog");
		alert.setContentText("Are you sure you want to go back?");
		
		Optional<ButtonType> result = alert.showAndWait();
		if(result.get() == ButtonType.OK){
			Stage app_stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
			
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/view/project2MainWindow.fxml"));
			AnchorPane root = (AnchorPane) loader.load();
			
			MainWindowController mainWindowController = loader.getController();
			mainWindowController.start(app_stage, thisApp, thisUser);
			
			Scene scene = new Scene(root, 600, 400);
			app_stage.setScene(scene);
			app_stage.show();
			app_stage.setTitle("Album View");
			app_stage.setResizable(false);
		}
	}
	
	/**
	 * Search photos.
	 *
	 * @param e the e
	 * @throws ParseException the parse exception
	 */
	@FXML
	public void searchPhotos(ActionEvent e) throws ParseException{ 
		
		//Search for either tags or by date
		String text1 = criteria1.getText();
		String text2 = criteria2.getText();
		ArrayList<Photo> matches = new ArrayList<Photo>();
		ArrayList<String>photoFilename = new ArrayList<String>();
		
		//create photo list that contains matches
		// check if checkboxes are checked? -- too many checks -__-
		
		if(searchingOnTags == true){
		
		System.out.println(allPhotos.size());
		for(int i = 0; i < allPhotos.size(); i++){
			System.out.println(i);
			for(int j = 0; j < allPhotos.get(i).getTags().size(); j++){
				System.out.println(allPhotos.get(i).getTags().get(j).getValue());
				if(allPhotos.get(i).getTags().get(j).getValue().equalsIgnoreCase(text2)){
					if(allPhotos.get(i).getTags().get(j).getType().equalsIgnoreCase(text1)){
					System.out.println("THERES A MATCH");
					matches.add(allPhotos.get(i));
					}
				}
			}
		}
		
		
		// add photo captions to new string list
		for(int a = 0; a < matches.size(); a++){
			photoFilename.add(matches.get(a).getFilename());
		}
		
		ObservableList<String> searchObsList = FXCollections.observableArrayList(photoFilename);
		
		listView.setItems(searchObsList);
		}
		
		else if(searchingOnDates == true){
			DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
			DateFormat df2 = new SimpleDateFormat("dd.MM.yyyy");
			Calendar fromDate = Calendar.getInstance();
			Calendar toDate = Calendar.getInstance();
			fromDate.setTime(df.parse(text1));
			toDate.setTime(df2.parse(text2));
			
			System.out.println(fromDate.getTime());
			System.out.println(toDate.getTime());
			for(int m = 0; m < allPhotos.size(); m++){
				Calendar photoDate = allPhotos.get(m).getDate();
				System.out.println(photoDate);
				if(toDate.after(photoDate) && fromDate.before(photoDate)){
					System.out.println("FOUND MATCH!");
					matches.add(allPhotos.get(m));
				}
			}
			
			for(int b = 0; b < matches.size(); b++){
				photoFilename.add(matches.get(b).getFilename());
			}
			
			ObservableList<String> searchObsList = FXCollections.observableArrayList(photoFilename);
			listView.setItems(searchObsList);
		}
		
	}
	
	/**
	 * Log out.
	 *
	 * @param e the e
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@FXML
	public void logOut(ActionEvent e) throws IOException{
		// go back to log in screen
		Alert alert = new Alert(AlertType.CONFIRMATION);
		 alert.setTitle("Confirmation Dialog");
		 alert.setContentText("Are you sure?");
		 
		 Optional<ButtonType> result = alert.showAndWait();
		 if(result.get() == ButtonType.OK){
			//serialize 
			   try {
					photoApp.writeApp(thisApp);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			 //Admin.writeApp(admin);
			 Stage app_stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
				
				FXMLLoader loader = new FXMLLoader();
				loader.setLocation(getClass().getResource("/view/login.fxml"));
				AnchorPane root = (AnchorPane) loader.load();
				
				LoginController loginController = loader.getController();
			      loginController.start(app_stage);

			      Scene scene = new Scene(root, 550, 358);
			      app_stage.setScene(scene);
			      app_stage.show(); 
			      app_stage.setTitle("Log in");
			      app_stage.setResizable(false);
		 }
	}
	
	/**
	 * Safe quit.
	 *
	 * @param e the e
	 */
	@FXML
	public void safeQuit(ActionEvent e){
		// quit program
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Confirmation Dialog");
		alert.setContentText("Are you sure you want to quit?");
		Optional<ButtonType> result = alert.showAndWait();
		
		if(result.get() == ButtonType.OK){
			//serialize 
			   try {
					photoApp.writeApp(thisApp);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			System.exit(0);
		}
	}
	
	/**
	 * Handle button action.
	 *
	 * @param e the e
	 */
	@FXML
	public void handleButtonAction(ActionEvent e){
		ArrayList<Photo> searched = new ArrayList<Photo>();
		
		if(searchDateCheckBox.isSelected()){
			//do something 
			searchingOnDates=true;
			searchingOnTags = false;
			criteria1.setText("From dd.MM.yyyy");
			criteria2.setText("To dd.MM.yyyy");
			
		}
		
		else if(searchTagCheckBox.isSelected()){
			// do something
			searchingOnTags=true;
			searchingOnDates = false;
			criteria1.setText("Type");
			criteria2.setText("value");
		}
	}
	
	/**
	 * All user photos.
	 *
	 * @param albums the albums
	 * @return the array list
	 */
	public ArrayList<Photo> allUserPhotos(ArrayList<Album> albums){
		ArrayList<Photo> returnPhotos = new ArrayList<Photo>();
		for(int i = 0; i < albums.size(); i++){
			for(int j = 0; j < albums.get(i).getPhotos().size(); j++){
				returnPhotos.add(albums.get(i).getPhotos().get(j));
			}
		}
		return returnPhotos;
	}
}
