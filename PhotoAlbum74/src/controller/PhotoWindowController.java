package controller;
/**
 * @author George Chronis Emmanuel Ankrah
 */
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;

import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.TilePane;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

import model.Album;
import model.User;
import model.photoApp;
import model.Photo;

import java.io.File;
import java.io.IOException;

import java.util.Optional;

// TODO: Auto-generated Javadoc
/**
 * The Class PhotoWindowController.
 */
public class PhotoWindowController {
	
	/** The add photo. */
	@FXML Button addPhoto;
	
	/** The copy to album. */
	@FXML Button copyToAlbum;
	
	/** The move to album. */
	@FXML Button moveToAlbum;
	
	/** The go back. */
	@FXML Button goBack;
	
	/** The copy album drop list. */
	@FXML ComboBox <Album> copyAlbumDrop;
	
	/** The move album drop list. */
	@FXML ComboBox <Album> moveAlbumDrop;
	
	/** The remove photo. */
	@FXML Button removePhoto;
	
	/** The display photo. */
	@FXML Button displayPhoto;
	
	/** The image pane. */
	@FXML TilePane imagePane;
	
	/** The scroll pane. */
	@FXML ScrollPane scrollPane;
	
	/** The display view. */
	@FXML ImageView displayView;
	
	/** The add photo field. */
	@FXML TextField addPhotoField;
	
	/** The caption. */
	@FXML Label caption;
	
	/** The date. */
	@FXML Label date;
	
	/** The tags. */
	@FXML Label tags;
	
	/** The safe quit. */
	@FXML Button safeQuit;
	
	/** The sign out. */
	@FXML Button signOut;
	
	/** The obs list. */
	private ObservableList<String> obsList;
	
	/** The clicked. */
	private ImageView clicked;
	
	/** The this app. */
	//public Admin thisAdmin;
	private photoApp thisApp;
	
	/** The selected photo. */
	private Photo selected;
	
	/** The this album. */
	private Album thisAlbum;
	
	/** The this user. */
	private User thisUser;
	
	/**
	 * Start.
	 *
	 * @param mainStage the main stage
	 * @param album the album
	 * @param app the app
	 * @param user the user
	 */
	public void start(Stage mainStage, Album album, photoApp app, User user){
		thisAlbum=album;
		thisApp=app;
		thisUser=user;
		obsList = FXCollections.observableArrayList();
//		   Calendar cal = Calendar.getInstance();
//		   cal.setTimeInMillis(0);
//		   cal.set(2016, 1, 1, 1, 1, 1);
//		   cal.set(Calendar.MILLISECOND,0);
//		  // Date date = cal.getTime(); // get back a Date object
//		   //album.addPhoto("//Users//George//Pictures//Starling_eggs");
//		   Photo photo = new Photo("file:///C://Users//George//Pictures//Starling_eggs.jpeg");
			//File file = new File("/Users/George/Pictures/Starling_eggs.jpeg");
//		   //Image image = new Image("file:///C:/Users/George/Pictures/Starling_eggs.jpeg", first.getLayoutX(), first.getLayoutY(), true, true);
//		  // Image img = new Image(file.toURI().toString(), 60, 50, true, true);
		//  String file = "/Users/George/git/photoalbum74/PhotoAlbum74/Data/1195016-unicornscover_super.jpg";
//		   String file2 = "/Users/George/git/photoalbum74/PhotoAlbum74/Data/Starling_eggs.jpeg";
//		   String file3 =  "/Users/George/git/photoalbum74/PhotoAlbum74/Data/Luscinia_cyane_-_Khao_Yai.jpg";
//		   String file4 =  "/Users/George/git/photoalbum74/PhotoAlbum74/Data/trifid_nebula.jpg";
//		   String file5 = "/Users/George/git/photoalbum74/PhotoAlbum74/Data/cone_nebula.jpg";
//		   String file6 =  "/Users/George/git/photoalbum74/PhotoAlbum74/Data/CEG_Lenses_6_original.jpg";
//		   String file7 =  "/Users/George/git/photoalbum74/PhotoAlbum74/Data/turner_slaveship-1840.jpg";
//		   String file8 =  "/Users/George/git/photoalbum74/PhotoAlbum74/Data/a_dense_swarm_of_ancient_stars.jpg";
//		   thisUser = new User("Blurp");
//		   thisUser.addAlbum("ONe");
//		   thisUser.addAlbum("Two");
//		   thisUser.addAlbum("Three");
//		   thisUser.addAlbum("OThree");
//		   thisUser.addAlbum("Four");
//		   thisUser.addAlbum("Five");
//		   thisUser.addAlbum("Six");
//getResource("/unibo/lsb/res/dice.jpg");
		// boolean photoBoo=  thisAlbum.addPhoto(new Photo("../../Data/cone_nebula.jpg"));
		//System.out.println(photoBoo);
//		thisAlbum = album;
//		thisAlbum.addPhoto(new Photo(file));
//		thisAlbum.addPhoto(new Photo(file2));
//		thisAlbum.addPhoto(new Photo(file3));
//		thisAlbum.addPhoto(new Photo(file4));
//		thisAlbum.addPhoto(new Photo(file5));
//		thisAlbum.addPhoto(new Photo(file6));
//		thisAlbum.addPhoto(new Photo(file7));
//		thisAlbum.addPhoto(new Photo(file8));
//		Tag tag = new Tag("city", "chicago");
//		thisAlbum.getPhotos().get(0).addTag(tag);
//		thisAlbum.getPhotos().get(0).setCaption("CAPTION");
//		thisAlbum.getPhotos().get(0).setDate(cal);
		// displayView.setImage( thisAlbum.getPhotos().get(0).getMainImage());
	        scrollPane.setFitToWidth(true);
	        //create image view array??
//	        ArrayList<ImageView> imgViews = new ArrayList<ImageView>();
//	        for(int i=0; i<thisAlbum.getNumberOfPhotos();i++){
//	        	imgViews.add(new ImageView(thisAlbum.getPhotos().get(i).getThumbnail()));
//	        }
//	        imagePane.getChildren().addAll(imgViews);
	        //UserList.setItems(obsList);
			
		   //loop through all photos and create  an image view for each and put the image views inside
		   // width 200 height 115
		  loadPhotos();
		  loadCombos();
		  mainStage.setTitle("Album View");
		  //load albums into dropdown lists
		   //1. Load all photos from album that was clicked on
		   //2. 
		   
	}
	
	/**
	 * Load combobox lists.
	 */
	public void loadCombos(){
		//empty out combos?
		copyAlbumDrop.getItems().clear();
		moveAlbumDrop.getItems().clear();
		//populate
		//process is the same for both. just load the names/albums for this user
		copyAlbumDrop.getItems().addAll(thisUser.getAlbums());
		moveAlbumDrop.getItems().addAll(thisUser.getAlbums());
		//select first item from the list
		copyAlbumDrop.getSelectionModel().select(0);
		moveAlbumDrop.getSelectionModel().select(0);
	}
	
	/**
	 * Load photos into tile view.
	 */
	public void loadPhotos(){
		//empty out scrollpane
		//imagePane.
		imagePane.getChildren().clear();
		//and repopulate
		 for(int i=0; i<thisAlbum.getNumberOfPhotos();i++){   
			 //reload from directory because images are not serializable
			 	thisAlbum.getPhotos().get(i).getImageFromDir();
			   ImageView imageView= new ImageView(thisAlbum.getPhotos().get(i).getThumbnail());
			   Photo currPhoto =thisAlbum.getPhotos().get(i);
			   Image currentImage =thisAlbum.getPhotos().get(i).getMainImage();
	           imageView.setFitWidth(70);
	           imagePane.getChildren().addAll(imageView);
	           imageView.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
              	  selected=currPhoto;
          		  clicked=imageView;
          		  //REPLACE MINI IMAGE IN PHOTO WITH APPROPRIATELY SIZED IMG AND SHOW in display window
          		//  ImageView imageV= new ImageView(thisAlbum.getPhotos().get(i).getMainImage());
          		  //clicked.getImage();
          		  displayView.setImage(currentImage);
          		  //show tags, caption, date
          		  caption.setText(currPhoto.getCaption());
          		  //I THINK TAGS COULD POTENTIALLY OVERRUN THE WINDOW!!!!
          		  date.setText(currPhoto.getDate().getTime().toString());
          		  tags.setText(currPhoto.getTags().toString());
                }
            });
		   }
	}
	
	/**
	 * Adds the photo.
	 *
	 * @param e the action event
	 */
	@FXML
	public void addPhoto(ActionEvent e){
		//add and update view 
		Alert alert = new Alert(AlertType.CONFIRMATION);
		 alert.setTitle("Confirmation Dialog");
		 alert.setContentText("Are you sure you want to add "+addPhotoField.getText() +"?");
		 
		 Optional<ButtonType> result = alert.showAndWait();
		 Photo photo = new Photo(addPhotoField.getText());
		 boolean success=false;
		 if(result.get() == ButtonType.OK){
			 success = thisAlbum.addPhoto(photo);
		 }
		 else{
			 return;
		 }
		if(!success){
			Alert alerto = new Alert(AlertType.INFORMATION);
			 //alert.initOwner(mainStage);
		       alerto.setTitle("Add photo error");
		       alerto.setHeaderText(  "Photo already exists");
		       alerto.setContentText("Photo alreayd exists, can't add again");
		   		alerto.showAndWait();
		}
		//UPDATE VIEW
		loadPhotos();
	}
	
	/**
	 * Removes the photo.
	 *
	 * @param e the action event
	 */
	@FXML
	public void removePhoto(ActionEvent e){
		//remove  and update view
		 if(selected==null){
			 Alert alert = new Alert(AlertType.ERROR);
			 alert.setTitle("No picture selected");
			 alert.setContentText("No picture selected to copy to album");
			 Optional<ButtonType> result = alert.showAndWait();
			 return;
		 }
		Alert alert = new Alert(AlertType.CONFIRMATION);
		 alert.setTitle("Confirmation Dialog");
		 alert.setContentText("Are you sure you want to delete "+selected.getFilename() +"?");
		 Optional<ButtonType> result = alert.showAndWait();
		//how do you get the image selected???
		boolean delete = false;
		if(result.get() == ButtonType.OK){
			delete =thisAlbum.deletePhoto(selected);
		}
		else{
			return;
		}
		if(!delete){
			Alert alerto = new Alert(AlertType.INFORMATION);
			//alert.initOwner(mainStage);
		    alerto.setTitle("Delete photo error");
		    alerto.setHeaderText(  "Photo doesn't exist");
		    alerto.setContentText("Photo doesn't exist");
		   	alerto.showAndWait();
		}
		//set selected to null
		selected=null;
		//update view...
		//maybe find imageview/photo
		loadPhotos();
		//clear display view of deleted photo
		displayView.setImage(null);
	}
	
	/**
	 * Display photo.
	 *
	 * @param e the action event
	 */
	@FXML
	public void displayPhoto(ActionEvent e){
		//right now this looks really bad, should just get rid of this if don't have time to make new display controller
//		displayView.setImage(clicked.getImage());
		 Stage app_stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
			
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/view/project2ImageDisplayWindow.fxml"));
			AnchorPane root = null;
			try {
				root = (AnchorPane) loader.load();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if(selected==null){
				Alert alerto = new Alert(AlertType.ERROR);
				 //alert.initOwner(mainStage);
			       alerto.setTitle("No photo selected");
			       alerto.setHeaderText(  "No photo selected");
			       alerto.setContentText("No photo selected");
			   		alerto.showAndWait();
			   		return;
			}
			ImageController loginController = loader.getController();
		      loginController.start(app_stage, selected, thisAlbum, thisUser, thisApp);

		      Scene scene = new Scene(root, 700, 600);
		      app_stage.setResizable(false);
		      app_stage.setScene(scene);
		      app_stage.show(); 
		      app_stage.setTitle("Image Display");
	}
	
	/**
	 * Safe quit.
	 *
	 * @param e the action event
	 */
	@FXML
	public void safeQuit(ActionEvent e){
		//reserialize everything in this user I guess
		 try {
				photoApp.writeApp(thisApp);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		 Alert alert = new Alert(AlertType.CONFIRMATION);
		 alert.setTitle("Confirmation Dialog");
		 alert.setContentText("Are you sure?");
		 Optional<ButtonType> result = alert.showAndWait();
		 if(result.get() == ButtonType.OK){
			 //serialize!!!!
			 Platform.exit();
			 System.exit(0);
		 }
		  
	}
	
	/**
	 * Sign out.
	 *
	 * @param e the action event
	 */
	@FXML  
	public void signOut(ActionEvent e){
		   //how do you sign out???
		   //reserialize and 
		try {
		photoApp.writeApp(thisApp);
	} catch (IOException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
		   //just return to log in window
		Alert alert = new Alert(AlertType.CONFIRMATION);
		 alert.setTitle("Confirmation Dialog");
		 alert.setContentText("Are you sure?");
		 
		 Optional<ButtonType> result = alert.showAndWait();
		 if(result.get() == ButtonType.OK){
			 Stage app_stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
				
				FXMLLoader loader = new FXMLLoader();
				loader.setLocation(getClass().getResource("/view/login.fxml"));
				AnchorPane root = null;
				try {
					root = (AnchorPane) loader.load();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				LoginController loginController = loader.getController();
			      loginController.start(app_stage);

			      Scene scene = new Scene(root, 550, 358);
			      app_stage.setResizable(false);
			      app_stage.setScene(scene);
			      app_stage.show(); 
			      app_stage.setTitle("Log in");
		 }
	}
	 
 	/**
 	 * Copy to album.
 	 *
 	 * @param e the action event
 	 */
 	@FXML
	 public void copyToAlbum(ActionEvent e){
		 if(selected==null){
			 Alert alert = new Alert(AlertType.ERROR);
			 alert.setTitle("No picture selected");
			 alert.setContentText("No picture selected to copy to album");
			 Optional<ButtonType> result = alert.showAndWait();
			 return;
		 }
		 Alert alert = new Alert(AlertType.CONFIRMATION);
		 alert.setTitle("Confirmation Dialog");
		 alert.setContentText("Are you sure you want to copy "+selected.getFilename() +" to " + copyAlbumDrop.getSelectionModel().getSelectedItem() + "?");
		 Optional<ButtonType> result = alert.showAndWait();
		
		 Album curr =null;
		 if(result.get() == ButtonType.OK){
		
			 //get album selected in dropdown list
			  curr =copyAlbumDrop.getSelectionModel().getSelectedItem();
			 //get the photo whose image is currently displayed
			 curr.addPhoto(selected);
		 }
	 }
	 
 	/**
 	 * Move to album.
 	 *
 	 * @param e the action event
 	 */
 	@FXML
	 public void moveToAlbum(ActionEvent e){
		 if(selected==null){
			 Alert alert = new Alert(AlertType.ERROR);
			 alert.setTitle("No picture selected");
			 alert.setContentText("No picture selected to copy to album");
			 Optional<ButtonType> result = alert.showAndWait();
			 return;
		 }
		 Alert alert = new Alert(AlertType.CONFIRMATION);
		 alert.setTitle("Confirmation Dialog");
		 alert.setContentText("Are you sure you want to copy "+selected.getFilename() +" to " + copyAlbumDrop.getSelectionModel().getSelectedItem() + "?");
		 Optional<ButtonType> result = alert.showAndWait();
		
		 Album curr =null;
		 if(result.get() == ButtonType.OK){
			//get album selected in dropdown list
			  curr =moveAlbumDrop.getSelectionModel().getSelectedItem();
			 //get the photo whose image is currently displayed
			//add photo to curr
			  curr.photosToString();
			 curr.addPhoto(selected);
			 curr.photosToString();
			 //delete photo from this current album
			 thisAlbum.photosToString();
			 thisAlbum.deletePhoto(selected);
			 thisAlbum.photosToString();
			 //update view
			  loadPhotos();
			  displayView.setImage(null);
		 }
	 }
	 
 	/**
 	 * Go back.
 	 *
 	 * @param e the action event
 	 */
 	@FXML
	 public void goBack(ActionEvent e){
		 Stage app_stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
			
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/view/project2MainWindow.fxml"));
			AnchorPane root = null;
			try {
				root = (AnchorPane) loader.load();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			MainWindowController pwController = loader.getController();
		      pwController.start(app_stage, thisApp, thisUser);

		      Scene scene = new Scene(root, 600, 400);
		      app_stage.setScene(scene);
		      app_stage.show(); 
		      app_stage.setTitle("Admin");
	 }
	   
}
