package controller;
/**
 * @author George Chronis Emmanuel Ankrah
 */
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import model.Album;
import model.User;
import model.photoApp;
import model.Photo;
import model.Tag;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

// TODO: Auto-generated Javadoc
/**
 * The Class ImageController.
 */
public class ImageController {
	
	/** The add tag. */
	@FXML Button addTag;
	
	/** The delete tag. */
	@FXML Button deleteTag;
	
	/** The back button. */
	@FXML Button backButton;
	
	/** The log out button. */
	@FXML Button logOutButton;
	
	/** The safe quit button. */
	@FXML Button safeQuitButton;
	
	/** The next image button. */
	@FXML Button nextButton;
	
	/** The previous image button. */
	@FXML Button prevButton;
	
	/** The tag type field. */
	@FXML TextField tagTypeField;
	
	/** The tag value field. */
	@FXML TextField tagValueField;
	
	/** The delete type field. */
	@FXML TextField deleteTypeField;
	
	/** The delete value field. */
	@FXML TextField deleteValueField;
	
	/** The delete tag field. */
	@FXML TextField deleteTagField;
	
	/** The caption field. */
	@FXML TextField captionField;
	
	/** The name re caption button. */
	@FXML Button nameReCaption;
	
	/** The image display. */
	@FXML ImageView  imageDisplay;
	
	/** The caption label. */
	@FXML Label captionLabel;
	
	/** The date label. */
	@FXML Label dateLabel;
	
	/** The tags label. */
	@FXML Label tagsLabel;
	
	/** The this photo. */
	private Photo thisPhoto;
	
	/** The this album. */
	private Album thisAlbum;
	
	/** The this user. */
	private User thisUser;
	
	/** The this app. */
	private photoApp thisApp;
	
	/**
	 * Start.
	 *
	 * @param mainStage the main stage
	 * @param photo the photo
	 * @param album the album
	 * @param user the user
	 * @param app the app
	 */
	public void start(Stage mainStage, Photo photo, Album album, User user, photoApp app){
		thisAlbum=album;
		thisPhoto = photo;
		thisApp=app;
		thisUser = user;
	//put image in image view
		imageDisplay.setImage(photo.getMainImage());
		//put caption, date, tags in labels
		captionLabel.setText(photo.getCaption());
		dateLabel.setText(photo.getDate().getTime().toString());
		tagsLabel.setText(photo.getTags().toString());
	}
	
	/**
	 * Next picture.
	 *
	 * @param e the action event
	 */
	@FXML 
	public void nextPic(ActionEvent e){
		//if there is a picture after this one, show it
//		if(){
//			
//		}
//		imageDisplay.setImage(thisPhoto.getMainImage());
//		//put caption, date, tags in labels
//		captionLabel.setText(thisPhoto.getCaption());
//		dateLabel.setText(thisPhoto.getDate().toString());
//		tagsLabel.setText(thisPhoto.getTags().toString());
		ArrayList<Photo> photoList = new ArrayList<Photo>();
		photoList = thisAlbum.getPhotos();
		//if(photoList.contains(thisPhoto)){
			int index = photoList.indexOf(thisPhoto);
			
			if(index  +1< photoList.size()){
				Photo nextPhoto = photoList.get(index + 1);
				imageDisplay.setImage(nextPhoto.getMainImage());
				captionLabel.setText(nextPhoto.getCaption());;
				dateLabel.setText(nextPhoto.getDate().getTime().toString());
				tagsLabel.setText(nextPhoto.getTags().toString());
				thisPhoto=nextPhoto;
			}
			
	}
	
	/**
	 *Slides to  Previous picture.
	 *
	 * @param e the action event
	 */
	public void prevPic(ActionEvent e){
		
		ArrayList<Photo> photoList = new ArrayList<Photo>();
		photoList = thisAlbum.getPhotos();
		//if(photoList.contains(thisPhoto)){
			int index = photoList.indexOf(thisPhoto);
			if(index > 0){
				
				Photo nextPhoto = photoList.get(index - 1);
				imageDisplay.setImage(nextPhoto.getMainImage());
				captionLabel.setText(nextPhoto.getCaption());;
				dateLabel.setText(nextPhoto.getDate().getTime().toString());
				tagsLabel.setText(nextPhoto.getTags().toString());
				thisPhoto=nextPhoto;
			}

	}
	
	
	/**
	 * Caption or repcaption a photo
	 *
	 * @param e the action event
	 */
	@FXML 
	public void nameReCaption(ActionEvent e){
		//both caption and recaption
		//either way just set it
		Alert alert = new Alert(AlertType.CONFIRMATION);
		 alert.setTitle("Confirmation Dialog");
		 alert.setContentText("Are you sure?");
		 Optional<ButtonType> result = alert.showAndWait();
		 if(result.get() == ButtonType.OK){
			thisPhoto.setCaption(captionField.getText());
			//and display it?
			captionLabel.setText(thisPhoto.getCaption());
		 }
	}
	
	/**
	 * Delete tag.
	 *
	 * @param e the action event
	 */
	@FXML
	public void deleteTag(ActionEvent e){
		//find a tag, if match, delete///////////////////////////////////////////////////////////////////////
		Alert alert = new Alert(AlertType.CONFIRMATION);
		 alert.setTitle("Confirmation Dialog");
		 alert.setContentText("Are you sure?");
		 Optional<ButtonType> result = alert.showAndWait();
		 if(result.get() == ButtonType.OK){
			 String key = deleteTypeField.getText();
				String value = deleteValueField.getText();
				Tag delete=thisPhoto.removeTag(new Tag( key,value ));
				
				if(delete==null){
					Alert alerto = new Alert(AlertType.INFORMATION);
					//alert.initOwner(mainStage);
				    alerto.setTitle("Delete tag error");
				    alerto.setHeaderText(  "Tag doesn't exist");
				    alerto.setContentText("Tag doesn't exist");
				   	alerto.showAndWait();
				}
				else{
					//display
					tagsLabel.setText(thisPhoto.getTags().toString());
				}
		 }
		 else{
			 deleteValueField.clear();
			 deleteTypeField.clear();
		 }
	}
	
	/**
	 * Adds the tag.
	 *
	 * @param e the action event
	 */
	@FXML
	public void addTag(ActionEvent e){
		//either have to split up sting somehow or put in two fields
		Alert alert = new Alert(AlertType.CONFIRMATION);
		 alert.setTitle("Confirmation Dialog");
		 alert.setContentText("Are you sure?");
		 Optional<ButtonType> result = alert.showAndWait();
		 if(result.get() == ButtonType.OK){
			String key = tagTypeField.getText();
			String value = tagValueField.getText();
			boolean add= thisPhoto.addTag(new Tag( key,value ));
			
			if(!add){
				Alert alerto = new Alert(AlertType.INFORMATION);
				//alert.initOwner(mainStage);
			    alerto.setTitle("Delete tag error");
			    alerto.setHeaderText(  "Tag already exists");
			    alerto.setContentText("Tag already exists");
			   	alerto.showAndWait();
			}
			//display it
			else{
				tagsLabel.setText(thisPhoto.getTags().toString());
			}
		 }
		 else{
			 tagValueField.clear();
			 tagTypeField.clear();
		 }
	}
	
	/**
	 * Back to thumbnail view.
	 *
	 * @param e the action event
	 */
	@FXML
	public void back(ActionEvent e){
		 Stage app_stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
			
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/view/project2PhotoViewWindow.fxml"));
			AnchorPane root = null;
			try {
				root = (AnchorPane) loader.load();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			PhotoWindowController pwController = loader.getController();
		      pwController.start(app_stage, thisAlbum, thisApp, thisUser);

		      Scene scene = new Scene(root, 600, 450);
		      app_stage.setScene(scene);
		      app_stage.show(); 
		      app_stage.setTitle("Album View");
		      app_stage.setResizable(false);
	}
	
	/**
	 * Safe quit.
	 *
	 * @param e the action event
	 */
	@FXML
	public void safeQuit(ActionEvent e){
		Alert alert = new Alert(AlertType.CONFIRMATION);
		 alert.setTitle("Confirmation Dialog");
		 alert.setContentText("Are you sure?");
		 Optional<ButtonType> result = alert.showAndWait();
		 if(result.get() == ButtonType.OK){
			 //serialize!!!!
		   try {
				photoApp.writeApp(thisApp);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			 Platform.exit();
			 System.exit(0);
		 }
	}
	
	/**
	 * Log out.
	 *
	 * @param e the action event
	 */
	@FXML
	public void logOut(ActionEvent e){
		   try {
				photoApp.writeApp(thisApp);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		   //just return to log in window
		   Alert alert = new Alert(AlertType.CONFIRMATION);
			 alert.setTitle("Confirmation Dialog");
			 alert.setContentText("Are you sure?");
			 
			 Optional<ButtonType> result = alert.showAndWait();
			 if(result.get() == ButtonType.OK){
				 //write everything to serializer
				 //writeApp();
				 Stage app_stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
					
					FXMLLoader loader = new FXMLLoader();
					loader.setLocation(getClass().getResource("/view/login.fxml"));
					AnchorPane root = null;
					try {
						root = (AnchorPane) loader.load();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					LoginController loginController = loader.getController();
				      loginController.start(app_stage);

				      Scene scene = new Scene(root, 661, 407);
				      app_stage.setResizable(false);
				      app_stage.setScene(scene);
				      app_stage.show(); 
				      app_stage.setTitle("Log in");
			 }
	}
}
