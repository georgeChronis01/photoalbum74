package controller;
/**
 * @author George Chronis Emmanuel Ankrah
 */
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import javafx.collections.FXCollections;




import model.User;
import model.photoApp;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;


// TODO: Auto-generated Javadoc
/**
 * The Class AdminController.
 */
public class AdminController {
	
	/** The User list. */
	@FXML ListView<User> UserList;
	
	/** The obs list. */
	private ObservableList<User> obsList;
	
	/** The add. */
	@FXML Button add;
	
	/** The delete. */
	@FXML Button delete;
	
	/** The Log out. */
	@FXML Button LogOut;
	
	/** The user. */
	@FXML TextField user;
	
	/** The bsafe quit. */
	@FXML Button bsafeQuit;
	
	/** The users. */
	//public Admin admin;
	private ArrayList<User> users;
	
	/** The this app. */
	private photoApp thisApp;
	 
 	/**
 	 * Start.
 	 *
 	 * @param mainStage the main stage
 	 * @param app the app
 	 * @throws IOException Signals that an I/O exception has occurred.
 	 */
 	public void start(Stage mainStage, photoApp app) throws IOException {
		 
		 thisApp=app;
		 obsList = FXCollections.observableArrayList();
		 //User admin = new User("admin");
//		 User test = new User("Test");
//		 User tes = new User("Tes");
//		 User tet = new User("Tet");
//		 User tst = new User("Tst");
//		 
//		 admini.addUser(test);
//		 admini.addUser(tes);
//		 admini.addUser(tet);
//		 admini.addUser(tst);
		 
		 obsList.addAll(thisApp.getUsers());

		 UserList.setItems(obsList);
		 UserList.getSelectionModel().select(0);
		 
		 // Listener
		 UserList
	      .getSelectionModel()
	      .selectedItemProperty()
	      .addListener(
	    		  (obs, oldVal, newVal) -> 
	    		  showItemInputDialog(mainStage));
	 }
	 
	 
	 /**
 	 * Show item input dialog.
 	 *
 	 * @param mainStage the main stage
 	 */
 	private void showItemInputDialog(Stage mainStage) {                
		   UserList.getSelectionModel().getSelectedItem();
		   UserList.getSelectionModel().getSelectedIndex();
	   }
	 
	 /**
 	 * Adds the user.
 	 *
 	 * @param e the action event
 	 */
 	@FXML
	 public void addUser(ActionEvent e){
		 // check to see if user name is not taken already
		 boolean isTaken = false;
		 for(int i = 0; i < thisApp.getUsers().size(); i++){
			 if(user.getText().equalsIgnoreCase(thisApp.getUsers().get(i).getName())){
				 Alert alert = new Alert(AlertType.ERROR);
				 alert.setTitle("Error Dialog");
				 alert.setHeaderText("Well this is bad");
				 alert.setContentText("There is a User that already has that name!");
				 Optional<ButtonType> result = alert.showAndWait();
				 if(result.get() == ButtonType.OK){
					 isTaken = true;
				 }
			 }
		 }
		 
		 if(isTaken == false){
			 User newUser = new User(user.getText());
			 thisApp.addUser(newUser);
			 obsList.add(newUser);
		 }
		 
	 }
	 
 	/**
 	 * Delete user.
 	 *
 	 * @param e the action event
 	 */
 	@FXML
	 public void deleteUser(ActionEvent e){
		 // delete a user selected from the observable list
		 //boolean canDelete = true;
		 
		int index = UserList.getSelectionModel().getSelectedIndex();
		 //get the user that is supposed to be deleted
		 User delete = UserList.getSelectionModel().getSelectedItem();
		 //if this user's name is admin, you can't delete
		 if(delete.getName().equalsIgnoreCase("admin")){
			 Alert delalert = new Alert(AlertType.ERROR);
			 delalert.setTitle("Error Dialog");
			 delalert.setHeaderText("Well this is bad");
			 delalert.setContentText("Cannot delete admin user!");
			 Optional<ButtonType> result = delalert.showAndWait();
//			 if(result.get() == ButtonType.OK){
//				 canDelete = false;
//			 }
			 
		 }
		 
		 else if(thisApp.getUsers().size() == 0){
			 Alert delalert = new Alert(AlertType.ERROR);
			 delalert.setTitle("Error Dialog");
			 delalert.setHeaderText("Well this is bad");
			 delalert.setContentText("At least one user must exist!");
			 Optional<ButtonType> result = delalert.showAndWait();
//			 if(result.get() == ButtonType.OK){
//				 canDelete = false;
//			 }
		 }
		 //if it's not the admin, delete it
		 else if(delete.getName().equalsIgnoreCase("admin") == false){
			
				 Alert alert = new Alert(AlertType.CONFIRMATION);
				 alert.setTitle("Confirmation Dialog");
				 alert.setContentText("Are you sure you want to delete this user?");
				 Optional<ButtonType> result = alert.showAndWait();
				 if(result.get() == ButtonType.OK){
					// int index = UserList.getSelectionModel().getSelectedIndex();
					 thisApp.getUsers().remove(delete);
					 obsList.remove(index);
					 UserList.getSelectionModel().select(index);
				 }
		 }
	 }
	 
	 /**
 	 * Logs out of admin window.
 	 *
 	 * @param e the action event
 	 * @throws IOException Signals that an I/O exception has occurred.
 	 */
 	public void logOutAdmin(ActionEvent e) throws IOException{
		 Alert alert = new Alert(AlertType.CONFIRMATION);
		 alert.setTitle("Confirmation Dialog");
		 alert.setContentText("Are you sure?");
		 
		 Optional<ButtonType> result = alert.showAndWait();
		 if(result.get() == ButtonType.OK){
			 //serialize!!!!
			 try {
				
				photoApp.writeApp(thisApp);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			 Stage app_stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
				
				FXMLLoader loader = new FXMLLoader();
				loader.setLocation(getClass().getResource("/view/login.fxml"));
				AnchorPane root = (AnchorPane) loader.load();
				
				LoginController loginController = loader.getController();
			      loginController.start(app_stage);

			      Scene scene = new Scene(root, 550, 358);
			      app_stage.setScene(scene);
			      app_stage.show(); 
			      app_stage.setTitle("Log in");
			      app_stage.setResizable(false);
		 }
	 }
	 
	 /**
 	 * Safe quit. 
 	 *
 	 * @param e the action event
 	 */
 	public void safeQuit(ActionEvent e){
		 Alert alert = new Alert(AlertType.CONFIRMATION);
		 alert.setTitle("Confirmation Dialog");
		 alert.setContentText("Are you sure you want to log off?");
		 Optional<ButtonType> result = alert.showAndWait();
		 
		 if(result.get() == ButtonType.OK){
			//serialize!!!!
			 try {
				photoApp.writeApp(thisApp);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			 System.exit(0);
		 }
 	 }
	 
}
